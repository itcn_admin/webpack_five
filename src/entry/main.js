import Vue from 'vue';
import Router from 'vue-router';
import '@/entry/main.scss';
import App from '@/entry/app.vue';
import router from '@/routes/index';
import store from '@/store/index';

Vue.use(router);
Vue.use(store);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })